tika (1.22-3) UNRELEASED; urgency=medium

  * Update debian/watch for new upstream tag layout

 -- tony mancill <tmancill@debian.org>  Wed, 14 Apr 2021 07:49:24 -0700

tika (1.22-2) unstable; urgency=medium

  * Cherrypick upstream commit to address CVE-2020-1950 and CVE-2020-1951
   (Closes: #954303, #954302)

 -- Moritz Muehlenhoff <jmm@debian.org>  Sun, 31 Jan 2021 22:18:47 +0100

tika (1.22-1) unstable; urgency=medium

  * New upstream release
    - Fixes CVE-2019-10088: A carefully crafted or corrupt zip file can cause
      an out of memory error in RecursiveParserWrapper (Closes: #933744)
    - Fixes CVE-2019-10094: A carefully crafted package/compressed file that,
      when unzipped/uncompressed yields the same file (a quine), causes a stack
      overflow error in RecursiveParserWrapper (Closes: #933746)
    - Fixes CVE-2019-10093: A carefully crafted 2003ml or 2006ml file could
      consume all available SAXParsers in the pool and lead to very long hangs.
      (Closes: #933745)
    - Refreshed the patches
    - Ignore the new dependency on c3p0 (not used)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 05 Aug 2019 11:41:25 +0200

tika (1.21-1) unstable; urgency=medium

  * New upstream release
    - Refreshed the patches
    - New dependency on libprotobuf-java
    - Ignore the build dependency on ossindex-maven-plugin
    - Ignore the new dependency on jakarta.activation (not used)
  * Standards-Version updated to 4.4.0

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 11 Jul 2019 22:48:11 +0200

tika (1.20-1) unstable; urgency=medium

  * New upstream release
    - Fixes CVE-2018-8017: Infinite loop in the IptcAnpaParser (Closes: #914643)
    - Refreshed the patches
    - New dependency on libgeronimo-annotation-1.3-spec-java
    - Depend on libapache-poi-java (>= 4.0)
    - New build dependency on libmaven-shade-plugin-java

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 22 Jan 2019 10:19:46 +0100

tika (1.18-1) unstable; urgency=medium

  * New upstream release
    - Fixes CVE-2016-4434: XML External Entity vulnerability (Closes: #825501)
    - Fixes CVE-2018-1339: Infinite loop in the CHM parser (Closes: #900000)
    - Refreshed the patches
    - Ignore the new dl, eval, langdetect and nlp modules
    - New dependencies on libcommons-exec-java, libjackson2-annotations-java,
      libjackson2-core-java, libjackson2-databind-java, libhttpmime-java,
      libjsoup-java, libuima-core-java, libandroid-json-org-java
      and libjson-simple-java
    - Depend on libpdfbox2-java instead of libpdfbox-java
    - Depend on librome-java (>= 1.6)
    - Depend on libapache-mime4j-java (>= 0.8.1)
    - Depend on libapache-poi-java (>= 3.17)
    - Ignore the new parsers with missing dependencies
  * Enabled the mp4 parser
  * Fixed the build failure with Java 11

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 20 Jan 2019 00:08:04 +0100

tika (1.8-1) unstable; urgency=medium

  * New upstream release
    - Refreshed the patches
    - Build the new tika-serialization and tika-batch modules
    - Fixed the compatibility with the version of metadata-extractor in Debian
    - New dependency on libcommons-csv-java
    - Depend on libapache-poi-java (>= 3.11)
  * Fixed the build failure with libmetadata-extractor-java 2.10
  * Merged and simplified the patches disabling unsupported parsers
  * Standards-Version updated to 4.3.0
  * Switch to debhelper level 11
  * Use salsa.debian.org Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 07 Jan 2019 00:56:25 +0100

tika (1.5-5) unstable; urgency=medium

  * Enabled JHighlight support
  * Depend on libasm-java (>= 5.0) instead of libasm4-java
  * Build with the DH sequencer instead of CDBS
  * Standards-Version updated to 3.9.8
  * Use a secure Vcs-Git URL
  * debian/watch: Track the release tags on GitHub

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 04 Oct 2016 15:38:26 +0200

tika (1.5-4) unstable; urgency=medium

  * Team upload.
  * Add 08-bouncycastle-1.51.patch and fix FTBFS with bouncycastle 1.51.

 -- Markus Koschany <apo@debian.org>  Sun, 06 Dec 2015 16:13:18 +0100

tika (1.5-3) unstable; urgency=medium

  * Team upload.
  * Add a new maven rule for bnd >= 2.1.0.
  * Tighten build dependency on bnd.
  * Vcs-Browser: Use https.

 -- Markus Koschany <apo@debian.org>  Sat, 21 Nov 2015 14:28:08 +0100

tika (1.5-2) unstable; urgency=medium

  * Depend on libmetadata-extractor-java >= 2.7.2
  * Tightened the dependency on libapache-poi-java
  * Added a build dependency on libvorbis-java
  * Standards-Version updated to 3.9.6 (no changes)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 01 Jun 2015 00:58:29 +0200

tika (1.5-1) unstable; urgency=medium

  * Initial release (Closes: #499606)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 17 Jul 2014 12:08:17 +0200
